<?php
include_once('DB_CONST.php');

function punktestand_bereits_submitted(){
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATABASE);
	$result = $mysqli->query('SELECT * FROM highscores WHERE game_id = "'. session_id().'";');
	return $result->num_rows > 0;
}

function echoTop10(){
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATABASE);
	$top10 = $mysqli->query('SELECT id, name, punkte, game_id FROM highscores ORDER BY punkte DESC, zeitpunkt DESC LIMIT 10');
	$rang = 0;
	
	$i = 0;
	echo '<p>Die Top 10:</p>';
	echo '<table><tr><th>Name</th><th>Punkte</th></tr>'; 
	while($row = $top10->fetch_assoc()){
		$i++;
		echo '<tr';
		if($row['game_id'] == session_id()){
			echo ' class=current-score';//bgcolor = "CornFlowerBlue"';
			$rang = $i;
		}
		echo '>';
		echo '<th>'.$row['name'].'</th><th>'.$row['punkte'].'</th></tr>';
	}
	echo '</table>';
	return $rang;
}
?>