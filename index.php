<!doctype html>
<html>
<head>
<link rel="stylesheet" href="style.css">
</head>
<body>
<?php
	session_set_cookie_params([
    'samesite' => 'Strict'
	]);
	session_start();
	session_regenerate_id();

?>

<div class="row">
	<div class="column">
		<div id="punkte"></div>

		<canvas id="spielfeld_canvas" width="400" height="400"></canvas>

		<script>
		window.onload=function() {
			spielfeld = document.getElementById("spielfeld_canvas");
			punktElem = document.getElementById("punkte");
			kontext=spielfeld.getContext("2d");
			anzahlTiles = 20;
			tileBreite = spielfeld.width / anzahlTiles;
			tileHoehe = spielfeld.height / anzahlTiles;
			spielerX = 5;
			spielerY = 5;
			geschwindigkeitX = 0;
			geschwindigkeitY = 0;
			schwanz = [];
			schwanz.push({x:spielerX, y:spielerY});
			laenge = 4;
			apfelX = 0;
			apfelY = 0;
			punkte = 0;
			setzeZufaelligeApfelPosition();
			eingabequeue = [];
			document.addEventListener("keydown", verarbeiteInput);
			spielschleifenIntervall = setInterval(spielschleife, 1000/15);
		}

		function spielschleife(){
			bewegeSpieler();
			ueberpruefeGameOver();
			ueberpruefeApfel();
			zeichneSpiel();
		}

		function bewegeSpieler(){
			aendereRichtung();
			spielerX += geschwindigkeitX;
			spielerY += geschwindigkeitY;
			schwanz.push({x:spielerX, y:spielerY});
			while(schwanz.length > laenge){
				schwanz.shift();
			}
		}

		function zeichneSpiel(){
			fuelleScreenSchwarz();
			zeichneSchlange();
			zeichneApfel();
			schreibePunkte();
		}

		function schreibePunkte(){
			punktElem.innerHTML = "Punkte: " + punkte;
		}

		function zeichneApfel(){
			kontext.fillStyle = "red";
			kontext.beginPath();
			kontext.arc((apfelX + 0.5) * tileBreite, (apfelY + 0.5) * tileHoehe, tileBreite*0.5, 0, 2 * Math.PI);
			kontext.closePath();
			kontext.fill(); 
		}

		function zeichneSchlange(){
			kontext.fillStyle = "lime";
			for(var i=0;i<schwanz.length;i++) {
				kontext.fillRect(schwanz[i].x*tileBreite,schwanz[i].y*tileHoehe,tileBreite,tileHoehe);
			}
		}

		function fuelleScreenSchwarz(){
			kontext.fillStyle = "black";
			kontext.fillRect(0,0,spielfeld.width,spielfeld.height);
		}

		function ueberpruefeApfel(){
			if(spielerX == apfelX && spielerY == apfelY){
				setzeZufaelligeApfelPosition();
				punkte += 10;
				laenge++;
			}
		}

		function ueberpruefeGameOver(){
		if(geschwindigkeitX != 0 || geschwindigkeitY != 0){
				if(spielerAusserhalbDerSpielflaeche()){
					spielEnde();
					return;
				}
				if(beisstSichSelbst()){
					spielEnde();
					return;
				}
			}
		}

		function beisstSichSelbst(){
			for(var i = 0; i < schwanz.length - 1; i++){
				if(spielerX == schwanz[i].x && spielerY == schwanz[i].y){
					return true;
				}
			}
			return false;
		}

		function spielerAusserhalbDerSpielflaeche(){
			return spielerX < 0 || spielerX >= anzahlTiles || 
				spielerY < 0 || spielerY >= anzahlTiles;
		}

		function spielEnde(){
			clearInterval(spielschleifenIntervall);
			ladeHighscoreEingabeseite();
		}

		function setzeZufaelligeApfelPosition(){
			apfelX = zufaelligerInt(anzahlTiles);
			apfelY = zufaelligerInt(anzahlTiles);
		}

		function zufaelligerInt(max){
			return Math.floor(Math.random() * max);
		}

		function verarbeiteInput(evt) {

			if(eingabequeue.length < 2){
				eingabequeue.push(evt.code);
			}
		}
		function aendereRichtung(){
			if(eingabequeue.length > 0){
				taste = eingabequeue.shift();
				switch(taste) {
					case "ArrowLeft":
						if(geschwindigkeitX != 1){
							geschwindigkeitX = -1;
							geschwindigkeitY = 0;
						}
						break;
					case "ArrowUp":
						if(geschwindigkeitY != 1){
							geschwindigkeitX = 0;
							geschwindigkeitY = -1;
						}
						break;
					case "ArrowRight":
						if(geschwindigkeitX != -1){
							geschwindigkeitX = 1;
							geschwindigkeitY = 0;
						}
						break;
					case "ArrowDown":
						if(geschwindigkeitY != -1){
							geschwindigkeitX = 0;
							geschwindigkeitY = 1;
						}
						break;
				}
			}
		}

		function ladeHighscoreEingabeseite()
		{
		   var myForm = document.createElement("FORM");
		   myForm.setAttribute("id", "Formular");
		   document.body.appendChild(myForm);
		   
		   var punkteInput = document.createElement("INPUT");
		   punkteInput.setAttribute("name", "punkte");
		   punkteInput.setAttribute("type", "hidden");
		   punkteInput.setAttribute("value", punkte);
		   document.getElementById("Formular").appendChild(punkteInput);
		   
		   myForm.method = "POST";
		   myForm.action = "enterName.php";
		   myForm.submit(); 
		}

		</script>
	</div>
	<div class="column">
		<?php
			include_once('DB_CONST.php');
			include_once('helper_functions.php');
			echoTop10();
			
		?>
	</div>
</div>
</body>
</html>