# snake-secdemo

## About
Snake-Spiel mit Datenbankanbindung zur Speicherung der Highscores.
Die Datenbankanbindung und der Code an sich weisen einige Sicherheitslücken auf. Diese sind absichtlich eingebaut. Das Projekt dient der Veranschaulichung dieser Sicherheitslücken.


## License
For open source projects, say how it is licensed.

